+++
title = "Croatian Summer"
tags = ["photography", "fujifilm", "xt2", "croatia", "iphone"]
image = "images/photography/210830-croatia/3_evening_at_rovinj.jpg"
date = "2021-08-30T10:03:26"
description = "Second year of Covid did not made vacation planning any easier. We threw our bike packing plans through southern france over board and joined friends in Istria instead."
+++

Second year of Covid did not made vacation planning any easier. We threw our bike packing plans through southern france over board and joined friends in Istria instead.

<!--more-->

That was the first trip where I took more pictures with my phone than with my Fuji. Of course, my aspiration for the pictures was not as high as when we traveled through Iceland. But the Iphone performed quite well in the different scenarios and I did not had to worry about salt water on my precious Fuji lenses.

![1_port_of_rovinj](../../images/photography/210830-croatia/1_port_of_rovinj.jpg)

The streets of Rovinj.

![2_streets_of_rovinj](../../images/photography/210830-croatia/2_streets_of_rovinj.jpg)

That is my definition of a car free town. 🤘🚲🤘

![4_rovinj_from_the_sea](../../images/photography/210830-croatia/4_rovinj_from_the_sea.jpg)

As romantic as it gets when you eat out in a harbor restaurant in Rovinj. Ignoring the hundreds of tourists behind me.

![3_evening_at_rovinj](../../images/photography/210830-croatia/3_evening_at_rovinj.jpg)

![5_biking_through_istria](../../images/photography/210830-croatia/5_biking_through_istria.jpg)

Biking is always fun when you stay away from the bigger roads.

![6_istria_forest](../../images/photography/210830-croatia/6_instria_forest.jpg)

We searched for a nice 15km walk through some valleys. It was advertised as waterfall hike with many possibilities to take a swim. But they did not say that there won't be any water in the end of August. In case you want to go there earlier in the year, here is the GPS track:

<iframe height='405' width='590' frameborder='0' allowtransparency='true' scrolling='no' src='https://www.strava.com/activities/5889630884/embed/70333b318d79ddccfc86dd6d8361b5ae6f60273e'></iframe>

![7_dry_rivers_in_istria](../../images/photography/210830-croatia/7_dry_rivers_in_istria.jpg)

The closing picture is probably one of the most cliche photo I shot.

![8_evening_at_istria](../../images/photography/210830-croatia/8_evening_at_istria.jpg)

This super short trip made it clear that Croatia is definitely not overated as a summer vacation destination. We will be back.
