+++
title = "White Mountain"
description = "An unprepared and totally unprofessional climb to a mountain top."
tags = ["photography", "fujifilm", "xt2", "austria"]
image = "images/photography/180603-whitemountain/whitemountain2-s.jpg"
date = "2018-06-03T10:25:22"
+++

Without any preparation or a defined route we tried to reach a mountain top near St. Johann in Tirol, Austria in March. More snow than expected and bad weather prevented us from achieving our ambitious goal.
<!--more-->
The pictures were taken with a Fujifilm XT-2 and the 18-55mm standard zoom. Editing was done in Exposure x3.

![Photo](../../images/photography/180603-whitemountain/whitemountain1.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain2.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain3.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain4.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain5.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain6.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain7.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain8.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain9.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain10.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain11.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain12.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain13.jpg)

![Photo](../../images/photography/180603-whitemountain/whitemountain14.jpg)
