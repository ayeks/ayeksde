+++
title = "First thoughts regarding Jekyll."
description = "How to list posts by category and highlight liquid tags."
tags = ["blog", "coding", "jekyll"]
date = "2015-07-19T10:25:22+05:30"
+++

Yesterday, I started with Jekyll as a sideproject. After searching a while for themes on [mademistakes](https://mademistakes.com/work/jekyll-themes/) and [jekyllthemes](http://jekyllthemes.org/) I found [Kaktus](https://github.com/nickbalestra/kactus). <!--more--> I tried some other themes too, but only kaktus seems as minimalistic and maintainable as I wanted it to be.

After deleting some Google Analytics and Disqus stuff I digged in the Liquid template engine to create some lists based on a parameter. To my astonishment, it was pretty easy:

## Create Jekyll post lists based on a parameter

The page:

```html
{% include archive-list.html category="photography" %}
```

The post:

```html
---
title:  "Some title"
category: photography
---
```

The include html:

```html
{% for post in site.posts %}
    {% if include.category %}
        {% if post.category == include.category %}
            <a href='{{ post.url }}'>{{ post.title }}</a>
        {% endif %}
    {% endif %}
{% endfor %}
```

## HowTo highlight Liquid tags

For this post I needed to include some code containing Liquid tags like {% raw %} {% include xyz %} {% endraw %}. I thought that I can print them with the {% raw %} {% highlight %} {% endraw %} tag without being processed. I was wrong. Neither DuckDuckGo nor Google could return some useful information to that problem. In the end, I found the solution in the Jekyll Sources itself where they describe the {% raw %} {% highlight %} {% endraw %} tag in the docs.
You simply have to surround the code by the tags **raw** and **endraw**.
