+++
title =  "Automate Awesome CV with xelatex and GitLab CI"
description = "I was tired of installing the complete LaTeX package - so I automated the LaTeX stuff in GitLab."
tags = ["blog", "coding", "LaTeX", "cicd"]
image = "images/blog/2018-01-25-awesome-cv-cicd/cv-s.jpg"
date = "2018-01-25T18:25:22+05:30"
+++

A few years ago I stumbled over the outstanding [Awesome CV](https://github.com/posquit0/Awesome-CV) which is written in LaTeX. At that time I had no idea what LaTeX - but customizing the CV to my needs, contributing the [Cover Letter](https://github.com/posquit0/Awesome-CV/commit/27657aa78f37a78be88406ac5c31311070eb3d37) and adding the image to the header taught me how it works. <!--more--> That was a good exercise because I had to write my Master Thesis in LaTeX.

## Using GitLab CI

Have a look at the [documentation](https://docs.gitlab.com/ce/ci/) if you have no idea what GitLab CI is. Everyone who knows LaTeX will probably complain now, because I could just use [Overleaf](https://www.overleaf.com/latex/templates/awesome-cv/dfnvtnhzhhbm) to edit my CV in the browser. Thats totally correct, but I want to use GitLab CI anyway.

I forked the official Awesome CV repository into [my own space on GitLab](https://gitlab.com/ayeks/Awesome-CV/tree/customizing). Everything that builds the CV automatically can be found in the [.gitlab-ci.yml file](https://gitlab.com/ayeks/Awesome-CV/blob/customizing/.gitlab-ci.yml).

```yaml
pdf:
  image: thomasweise/texlive
  script:
    - cd examples
    - xelatex resume.tex
  artifacts:
    paths:
    - examples/resume.pdf
```

By adding this file into the root directory of the repository a GitLab runner will execute the defined tasks. *pdf* defines the name of the runner job. The runner starts the [docker image](https://hub.docker.com/r/thomasweise/texlive/) that has texlive installed: *image: thomasweise/texlive*. Everything in *script* will be executed in the docker container. Just like we use xelatex on the local machine - the runner will switch to the *examples* folder and execute *xelatex resume.tex*. That will generate a PDF file - if no error occurred.
To obtain the generated PDF file we have to define the file as *artifact*. The artifact can be downloaded if the job has finished successful.

![Photo](../../images/blog/2018-01-25-awesome-cv-cicd/job.png)

The jobs for my repository can be found [here](https://gitlab.com/ayeks/Awesome-CV/-/jobs).

In the [README.md](https://gitlab.com/ayeks/Awesome-CV/blob/customizing/README.md) the status of the pipeline is displayed and direct links to the artifacts can be found. Have a look at the official documentation for the [badges](https://docs.gitlab.com/ce/user/project/pipelines/settings.html#badges).

```yaml
# Customized Resume / CV

Pipeline Status: [![pipeline status](https://gitlab.com/ayeks/Awesome-CV/badges/customizing/pipeline.svg)](https://gitlab.com/ayeks/Awesome-CV/commits/master)

[Browse](https://gitlab.com/ayeks/Awesome-CV/-/jobs/artifacts/customizing/browse?job=pdf) the Artifacts or [download](https://gitlab.com/ayeks/Awesome-CV/-/jobs/artifacts/customizing/raw/examples/resume.pdf?job=pdf) the PDF directly.

```

![Photo](../../images/blog/2018-01-25-awesome-cv-cicd/readme.png)

If you want to build your own CV with gitlab-ci feel free to fork [my repository](https://gitlab.com/ayeks/Awesome-CV/tree/customizing) and start editing the files in the *customizing* branch.
