+++
title = "Master Thesis: TresorSGX"
description = "Securing storage encryption by using Intel SGX enclaves. First attempt for the isolation of OS components with trusted enclaves. Nominated for the CAST-Förderpreis and published at SysTEX'16."
tags = [ "blog", "coding", "linux", "kernel", "intelsgx", "research" ]
date = "2016-12-15T10:25:22"
image = "images/blog/2016-12-15-tresorsgx/tresorsgx_arch.png"
+++

In my last year at university (2015-2016) I developed a linux kernel module which secures the Linux Crypto API using the Intel Software Guard Extensions. That was not a trivial task because multiple difficulties occured during the design and implementation. In the end the result was pretty solid and I was nominated for the [CAST-Förderpreis IT-Sicherheit 2016](https://www.cast-forum.de/en/award/award.html) in Darmstadt.<!--more--> If you like you download my thesis [here](../../images/blog/2016-12-15-tresorsgx/tresorsgx.pdf). The work was also published in the [paper Isolating Operating System Components with Intel SGX](https://dl.acm.org/citation.cfm?doid=3007788.3007796) at the [SysTEX'16](https://systex.ibr.cs.tu-bs.de/systex18/).

> In the following I quote the readme of my [GitHub repository of TresorSGX](https://github.com/ayeks/TresorSGX). Feel free to fork the repo and improve the system. Drop me a line if you have any questions.

## TresorSGX

### Introduction

With release of the first CPUs with [Intel Software Guard Extensions](https://software.intel.com/en-us/sgx) in October 2015 and the [Intel SGX SDK](https://software.intel.com/en-us/sgx-sdk) in the first half of 2016, a new technology was made available to execute code in a secured *enclave*. These protected enclaves are shielded against access and modification of any outside application - privileged or not.

*TresorSGX* is an attempt to isolate and secure a operating system component of the Linux kernel. It outsources a functionality of a kernel module into a SGX container / enclave. TresorSGX provides a cryptographic algorithm for the Linux Crypto API which is executed in the secured container. **The cryptographic key material is guarded from unauthorised access of unprivileged and privileged components at any time.** This protects the disk-encryption system from cold-boot and DMA attacks on the key material.

TresorSGX was built during my master thesis at the [IT-Security chair](https://www1.cs.fau.de/) of the Department of Computer Science at the Friedrich-Alexander University (FAU) Erlangen Nuremberg. Have a look at [https://www1.cs.fau.de/sgx-kernel](https://www1.cs.fau.de/sgx-kernel) for more information about the thesis.

## Background: Intel SGX

If you are familiar with the Intel SGX Architecture and its security characteristics, you can skip this section. Otherwise it is highly recommended to read through to develop an understanding of the design decisions we made. If you want a more in-depth understanding of Intel SGX - besided the official documentation - I recommend the paper [Intel SGX explained by Costan and Devadas](https://eprint.iacr.org/2016/086).

### Background: Intel SGX Security Characteristics

Intel also proposed [methods to use SGX to deploy trustworthy software solutions](https://software.intel.com/sites/default/files/article/413938/hasp-2013-innovative-instructions-for-trusted-solutions.pdf). This is achieved by using [attestation, provisioning and sealing techniques](https://software.intel.com/en-us/articles/innovative-technology-for-cpu-based-attestation-and-sealing).

![Photo](../../images/blog/2016-12-15-tresorsgx/sgx_secured_enclave.png "Secured Enclave")

The enclave memory is secured against observation and modification of any non-enclave party. That excludes virtual machine monitors, ring-0 applications or other enclaves. This is achieved by encrypting the memory with a [in-CPU Memory Encryption Engine (MME)](https://software.intel.com/en-us/blogs/2016/02/26/memory-encryption-an-intel-sgx-underpinning-technology).

Via a hard-coded private key the CPU is able to perform an attestation of itself against a challenger and to sign via public-key cryptography a measurement of an enclave. That can be used to guarantee the integrity of an enclave and for enclave attestation.

Function calls into the enclave are provided via special instruction which perform checks on the callee and the function arguments. The same applies for function calls from the enclave to the outside. Interrupts and unplanned exits will not reveal secure information because an enclave can only be stopped in a secured area.

SGX allows the usage of multiple enclave instances which are isolated against each other and from the system software.

### Background: Intel SGX Architecture

The Intel Software Guard Extensions consist of multiple parts. The basis builds the Intel Skylake CPU with its extended instruction set and memory access mechanisms. These instructions are used to create, launch, enter and exit an enclave. The protected memory, the Enclave Page Cache (EPC), for the enclave is allocated in the Processor Reserved Memory (PRM) and secured with a Memory Encryption Engine.

![Photo](../../images/blog/2016-12-15-tresorsgx/sgx_arch_highlevel.png "Intel SGX Archicture")

The untrusted host application can call trusted functions inside the enclave. Neither the input to the enclave, nor the output of the enclave can be fully trusted because a malicious OS can modify these channels. The enclave author has to take this into consideration developing security critical applications. To initiate the enclave a launch token is needed which can be retrieved with the help of the Intel Launch Enclave. The access to the Launch Enclave and other architectural enclaves (Quoting, Provisioning, etc) is provided by the AESM service in user space. SGX libraries provide the necessary methods to communicate with the AESM Service. **Enclaves can only be entered in user space.** However, creating and initiating an enclave is only possible in kernel space. Therefore, a privileged SGX module or driver must be installed in kernel space to manage the enclave page cache and calling the specific SGX instructions. The launched enclave can only be entered from an unprivileged user-mode application via special SGX instructions.

## TresorSGX Design

As previously described it is not possible to enter an enclave from kernel space. An enclave’s code has always to be executed in ring three with a reduced set of allowed instructions and a limited amount of available memory. To overcome these major limitations of SGX, it was decided to build an architecture which moves part of the kernel functionality to user space such that the core functionality can then be wrapped by an enclave. This enclave is implemented by a user space service or daemon which calls the Intel Launch enclave for initialisation. Once the enclave is running, functionality within the enclave can be used by the daemon. Consequently, the kernel first has to communicate with the daemon which then passes the request to the enclave.

![Photo](../../images/blog/2016-12-15-tresorsgx/tresorsgx_arch.png "TresorSGX Archicture")

TresorSGX is an exemplary implementation in the scope of full disk encryption. The TresorSGX LKM registers a new cipher within the crypto API of the Linux kernel which can then be used by dm-crypt. The encryption algorithm used for full disk encryption is implemented within an enclave, and thus it is guaranteed that the implementation cannot be tampered with. The key used for disk encryption is securely derived within the enclave from a password chosen by the user and a device specific salt. The user password can be entered with the help of a tool which communicates with the daemon directly in user mode and the salt is stored sealed to the enclave identity. Consequently, it cannot be unsealed on a different device

### TresorSGX Workflow

The overall functionality of the implementation is spread between the LKM and the user space daemon.

**Initialising LKM and daemon:**
When the kernel module is initialized, it first registers a Netlink family for the communication with the daemon. Once the Netlink socket is created, it starts the daemon via the user mode helper API. The daemon then creates and starts the enclave. Using the key setting functionality of the crypto API would leak the key or password to main memory, therefor a possibility is provided to directly set the password using only the daemon.

**Deriving disk encryption key:**
After the password has been read from the user, the daemon loads a predefined file from disk which contains the sealed salt. The enclave checks if the sealed salt is valid and unseals it. If the sealed data is not valid, it will generate a new salt and seal it. The Password-Based Key Derivation Function 2 (PBKDF2) is used to finally derive the disk encryption key from the user password and the salt.

**Establish Netlink communication:**
The daemon creates the same Netlink interface as the kernel module and sends an initialisation succeeded message to the kernel. The kernel receives the message and registers the new cipher at the crypto API.

**Data encryption and decryption:**
After initialization, the encryption or decryption process is straight forward. The encrypt and decrypt callback functions of our LKM are called by the user of the crypto API. The LKM then sends a Netlink message to the daemon, which calls the encrypt and decrypt functions of the enclave. The enclave performs the requested cryptographic operation and returns the encrypted or decrypted block which is passed back to the kernel via Netlink. Finally, the kernel module copies the block to the destination given by the caller of the crypto API and returns.

## Usage

### TresorSGX requirements

Because the new CPU instructions of the Intel Software Guard Extensions only current Intel CPU are able to execute the TresorSGX enclave. Have a look at the [SGX-hardware list](https://github.com/ayeks/SGX-hardware) for more information about SGX support.
The requirements are:

1. a SGX capable CPU
1. SGX must be enabled in BIOS (is default off)
1. [Intel SGX Linux SDK](https://software.intel.com/en-us/sgx-sdk/download) must be installed
1. Intel SGX AESM Daemon must be running (it is running by default if you installed the SDK)

## Contributing

The main drawback of TresorSGX in its disk encryption use case is the low performance. It operates at 1% of the standard AES implementation due the big overhead of the Netlink communication. The performance analysis provides more information regarding that topic. If someone is able to replace or to improve the Netlink communication I am happy to support him on his way.
