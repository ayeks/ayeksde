+++
title =  "Neuronale Korrelate des Framing Effekts und die Brücke zur Prospect Theory"
description = "Welche Hintergründe hat der Framing Effekt, wie beeinflusst er unser Handeln und wie können wir mit ihm das Handeln Anderer beeinflussen."
tags = [ "blog", "paper", "marketing", "research" ]
date = "2014-10-14T18:25:22"
+++

Der Framing Effekt beschreibt die unterschiedliche Auffassung eines Problems bei unterschiedlicher Darstellung. Man kann den Effekt bei Finzanzspekulationen und dem Preisempfinden beobachten. Das Paper wurde für ein Marketing Seminar an der Friedrich-Alexander Universität Erlangen-Nürnberg angefertigt.<!--more-->

Das Paper kann hier gelesen werden: [Paper](../../images/blog/2014-10-25-neuronale-korrelate/neuronale-korrelate.pdf)

### Einleitung

Mit dem Definition des Framing Effektes stellten Amos Tversky und Daniel Kahneman 1981 eine Erklärung für das bisher unerklärliche nicht rationale Handeln von Menschen her. Die Erkenntnisse wurden aus den Studien zur Prospect Theory gewonnen, welche die Entscheidungsfindung bei ungewissen Risiken beschreibt.

De Martino et al. untersuchten die neuronalen Aktivitäten im menschlichen Gehirn während den Testpersonen verschiedene Probleme entsprechend dem Framing Effekt vorgelegt wurden. Ihre Erkenntnisse geben Aufschluss dazu, warum Menschen für das Framing so anfällig sind und warum es durchaus evolutionär von Vorteil ist. In dieser Arbeit wird der Framing Effekt vorgestellt und anhand den Normativen Regeln erläutert. Im weiteren werden die Neuronalen Korrelate des Framing Effektes dargestellt und somit die neuronalen Hintergründe aufgezeigt.

Abschließend wird mit der Prospect Theory das nicht-rationale Handeln bewiesen und verschiedenen Beispielen der Anwendungsbezug zur realen Welt hergestellt.
