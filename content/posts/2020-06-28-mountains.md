+++
title = "Bavarian Mountains"
tags = ["photography", "fujifilm", "xt2", "germany", "bavaria"]
image = "images/photography/200628-mountains/Photo_1461.jpg"
date = "2020-06-28T10:25:22"
description = "Originally we planned to experience the south-african heat this year. However, COVID-19 forced us to focus more on our local bavarian nature which is worth a trip without a doubt."
+++

Originally we planned to experience the south-african heat this year. However, COVID-19 forced us to focus more on our local bavarian nature which is worth a trip without a doubt.

<!--more-->

The following images are some black and white exports of my Fuji. I also thought about
pushing them to [my Instagram](https://www.instagram.com/larslurres/) as well, or even
creating a second instagram account for black and white images only. But the closed
walled garden of the Facebook ecosystem anoys me more and more. The only reason I still
post images there, is probably because of the sunken cost fallacy.
Nevertheless, enjoy the pictures:

![Photo](../../images/photography/200628-mountains/Photo_1436.jpg)

We stays close to the Zugspitze and the Eibsee. It was the first warm weekend after the lockdown
opening. Therefor many tourists from Munich excaped the city heat into the mountains.

![Photo](../../images/photography/200628-mountains/Photo_1441.jpg)

![Photo](../../images/photography/200628-mountains/Photo_1445.jpg)

![Photo](../../images/photography/200628-mountains/Photo_1449.jpg)

The wildlife is always interesting. Especially when you stumble over alpacas.

![Photo](../../images/photography/200628-mountains/Photo_1451.jpg)

![Photo](../../images/photography/200628-mountains/Photo_1458.jpg)

We planned hiking tours for every day ahead but ended up with changing them at the evening
before them every time. On our first tour we went to the border of Austria. Checkout the GPS info
on [Strava](https://www.strava.com/activities/3552690476).

![Photo](../../images/photography/200628-mountains/Photo_1461.jpg)

![Photo](../../images/photography/200628-mountains/Photo_1470.jpg)

![Photo](../../images/photography/200628-mountains/Photo_1481.jpg)

On our last day it was a little bit rainy. So we packed everything together and drove as early
as possible to Schloss Neuschwanstein. We were basically on our own there because no foreign tourists
where in the area. The [walk arount the Alpsee](https://www.strava.com/activities/3561950211) was
really calm and beautiful.

![Photo](../../images/photography/200628-mountains/Photo_1483.jpg)

We did not buy any cards in advance. Because of Corona
they did tours with only 10 people per group, instead of the usual 50.

![Photo](../../images/photography/200628-mountains/Photo_1485.jpg)
