---
title: 'About'
image: images/about.png
url: "about"
menu:
  main:
    name: "About"
    weight: 3
---

#### Who am I?

I am an amateur photographer, an software engineer, an information security specialist and a DevOps platform architect. I studied computer science, specialized to distributed systems and security at the Friedrich-Alexander University Erlangen-Nuremberg. At Siemens, I design, implement and operate the platform for our Rail and Infrastructure assets.

#### What have I done in the past?

The compulsory military service in **2009**, right after my A-levels, helped me a lot to learn about people, management and what I definitely do not want to do in my life. I was really lucky that I served in an active group that plotted training events against divisions that are going to war zones the next month. Most of the times we were in the field, waiting for an attack to happen, to attack other bases or to hunt enemy tanks. As much as I liked the dense "Teamgeist" we established there, I knew that this life is not the one I want for me.

In **2010** I started my studies for applied informatics in a smaller university near Halle / Saale. During the Bachelor studies I did some research on networking and spoofing techniques which sparked my interest in security. However, the content of the most lectures were business oriented - so I was only able to get more into the field of information security by self studying.

In October **2013** I started my Master at the FAU Erlangen-Nuremberg and the complexity and diversity of the available courses overwhelmed me. I only had a blurry understanding of the direction I wanted to approach. I started introduction courses into InfoSec and Distributed Systems. The technical detail of those courses was astonishing. In the middle of my Masters, after courses like Information Security, Virtual Machines, Cloud Technologies and many others, I wanted to keep on studying forever.

Because the big cities of Bavaria are super expensive, I had to work 20h besides studying. When looking for a job I tried to not go to the big local companies like Datev, Bosch, Adidas or Siemens because I wanted to avoid the corporate BS world. However money was really an issue, so I joined Siemens as a working student. I was really lucky that my boss there gave my the possibility to implement state of the art logging and monitoring systems in their high security data center which helped me a lot to learn data center operation basics.

The idealism to stay in university forever changed after **10 month** of in depth Linux Kernel development where I created an kernel encryption module which makes use of an, at that time, undocumented CPU extension. I learned that the deep technical and foundational work is somehow fascinating, but in the end not that rewarding. Therefore I focused on entering the enterprise InfoSec world - which succeeded in Summer **2016**.

My first station in the IT graduate program at Siemens was located in an InfoSec department that supports a business division. All in all we were responsible for the security of different factories and development teams. In a really interesting project we tried to map ISO27001 controls to DevOps processes and cloud native services.

In the **second year** of the graduate program I was supporting another business division. During that time I gained more security knowledge by studying for and taking the CISSP and CCSP exams. Being in another division it became clear that the problems are still the same. IT has no budget, must outsource everything to low cost countries and nobody has time for security.

At the end of **2018** there was a hiring stop for technical experts in Germany in Siemens central departments. My only option after the graduate program was to switch to a project manager role for the outsourcing of cyber security projects. At this point in my life I was not already satisfied enough with my technical expertise, so that I decided to ditch the central security departments and get into the messy business side of things. In hindsight, that was the best decision I could have taken back then.

I started in **2019** with the overloaded title of a Cloud Security DevOps Engineer at the Siemens Mobility division where I actively drove the implementation of security standards and processes in customer services development teams. We adopted DevSecOps methodologies wherever possible to generate as much insights from the data that is send to us by our trains. My role was mostly the translation of ISO27001 requirements into something that is more than just a security theater in practice.

We experienced first hand how it feels when you try to do fancy data science in a world where you need to deliver products that run governmental approved software which needs to run for 30 years. In **2021** I transitioned more into a role of a DevOps Platform Architect because more and more teams got onboarded onto the platform but the platform team did not scale with it. So we had to support more teams without more people and without sacrificing security.

**Currently** I am still bridging ISO27001 topics into the development and platform engineering world but I am far more invested into defining the future architecture for our services. With the fast changing and breaking world of Kubernetes on one side and long-running cost-sensitive service contracts on the other side, it is quite an interesting place to be at right now.
