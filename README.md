# ayeks.de

[![pipeline status](https://gitlab.com/ayeks/ayeksde/badges/master/pipeline.svg)](https://gitlab.com/ayeks/ayeksde/commits/master)

This is the repo for my private website [ayeks.de](http://ayeks.de).

## Local Usage

Run hugo: `hugo server -v`
